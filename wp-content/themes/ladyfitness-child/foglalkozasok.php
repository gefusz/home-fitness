<?php
/*
	Template Name: Foglalkozasok
*/
?>

<?php
	include "header.php";
?>
		<main>
			<section class="foglalkozasok">
				<div class="container content-pull">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="text-center">foglalkozásaink</h2>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-1">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">fitneszezőknek</h3>
						    <ul>
									<li><p class="card-text text-center">amit akarsz óra</p></li>
									<li><p class="card-text text-center">kick-box aerobik</p></li>
									<li><p class="card-text text-center">zumba fitness</p></li>
									<li><p class="card-text text-center">stretching</p></li>
									<li><p class="card-text text-center">kondi step</p></li>
									<li><p class="card-text text-center">alakformáló óra</p></li>
									<li><p class="card-text text-center">capoeira aerobik</p></li>
									<li><p class="card-text text-center">body toning</p></li>
						    </ul>						    
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-2">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						  	<h3 class="card-title text-center">gyógytorna, pilates, gerinctorna</h3>
						    <ul>
									<li><p class="card-text text-center">stretching</p></li>
									<li><p class="card-text text-center">duci torna</p></li>
									<li><p class="card-text text-center">senior torna</p></li>
									<li><p class="card-text text-center">gerinctorna</p></li>
									<li><p class="card-text text-center">pilates</p></li>
									<li><p class="card-text text-center">dinamikus jóga</p></li>
									<li><p class="card-text text-center">gerincjóga</p></li>
									<li><p class="card-text text-center">deepwork</p></li>
						    </ul>	
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-3">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">terhestorna, kismamatorna</h3>
						    <ul>
									<li><p class="card-text text-center">jóga alapú kismamatorna</p></li>
									<li><p class="card-text text-center">terhestorna</p></li>
									<li><p class="card-text text-center">kismamajóga</p></li>
						    </ul>	
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-4">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">babásoknak</h3>
						    <ul>
									<li><p class="card-text text-center">alakformáló babával</p></li>
									<li><p class="card-text text-center">babás jóga</p></li>
									<li><p class="card-text text-center">baba-mama torna</p></li>
						    </ul>	
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-5">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">gyermekekkel</h3>
						    <ul>
									<li><p class="card-text text-center">manó ringázó</p></li>
									<li><p class="card-text text-center">manónéptánc</p></li>
									<li><p class="card-text text-center">manó muzsika</p></li>
									<li><p class="card-text text-center">manó torna</p></li>
						    </ul>	
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="foglalkozasok-card-6">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">ovisoknak, kisiskolásoknak</h3>
						    <ul>
									<li><p class="card-text text-center">gigacsigabiga táncelőkészítő</p></li>
									<li><p class="card-text text-center">kölyök fitness</p></li>
									<li><p class="card-text text-center">tini aerobic</p></li>
						    </ul>	
						  </div>
						</div>
					</div>
				</div>
			</section>
		</main>
		
<?php
	include "footer.php";
?>	