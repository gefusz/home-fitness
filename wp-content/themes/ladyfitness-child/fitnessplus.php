<?php
/*
	Template Name: Fitnessplus
*/
?>

<?php
	include "header.php";
?>
		<main>
			<section class="fitnessplus">
				<div class="container content-pull">
					<div class="row">	
						<div class="card col-12">
							<h2 class="text-center">fitness plus szolgáltatásaink</h2>
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">Kismama masszázs</h3>
						  <div>
						  	<p class="text-center">A masszázs hatására enyhül a fáradtságérzet, az izmok rugalmassága nő, csökken a kóros folyadék-visszatartás, és oldódnak az izomgörcsök. A kezelés révén a nyirokrendszer is megtisztul, ami különösen azért fontos, mert a terhesség során…</p>
						  </div>
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">női személyi edző</h3>
						  <div>
						  	<p class="text-center">Gondolatok a személyi edzőről: "A jó személyi edző motiváló erő, szakember, lélekgyógyász, tanár és jó társaság egy személyben, akinek feladata, hogy megkönnyítse, hatékonnyá és nem utolsósorban felüdüléssé varázsolja az edzéseidet."</p>
						  </div>
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">infravit philips vitae infrakabin</h3>
						  <div>
						  	<p class="text-center">Az InfraVit infrakabin különösen ajánlott Önnek, ha jobban szereti vagy jobban bírja az alacsonyabb hőmérsékletet, mint a 100 fokos szaunát, mégis élvezni szeretné az izzasztófürdő jótékony hatásait, izzadni akar, és emellett élvezni szeretné az infravörös…</p>
						  </div>						  
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">gyermekmegőrző</h3>
						  <div>
						  	<p class="text-center">Hétfő - Péntek: 8:30 - 12:00-ig</p><p class="text-center">Hétvégi és egyéb más időpontú gyermekfelügyeleti igény esetén kérjetek információt a recepción vagy telefonon illetve az info kukac noifitnesz pont hu e-mail címen. Gyermekfelügyeletet az edzőterem gyermekmegőrzőben 6 hónapostól 7 éves korig Havi…</p>
						  </div>
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">szép kártya</h3>
						  <div>
						  	<p class="text-center">Mi az a SZÉP kártya? Egy olyan új, innovatív termék, amely vendéglátóhelyi étkezési, üdültetési és rekreációs célú béren kívüli juttatások folyósítását és felhasználását teszi lehetővé elektronikus utalvány formájában. A SZÉP kártya jelentősen gyorsítja és biztonságosabbá…</p>
						  </div>
						</div>
						<div class="card col-lg-4 col-md-12">
							<h3 class="text-center">power step plus - vibrációs edzés</h3>
						  <div>
						  	<p class="text-center">Power Step Plus - A sportos tréner</p> <p class="text-center">A vibrációs edzés nagy előnye a hagyományos edzésekkel szemben, hogy nem csak az erőt növeli, hanem az izmok rugalmasságát is. Az izomerő növekedése gyakran az izmok rövidülésével jár…</p>
						  </div>
						</div>
					</div>
				</div>
			</section>
		</main>
		
<?php
	include "footer.php";
?>	