<?php
/*
	Template Name: Főoldal
*/
?>

<?php
	include "header.php";
?>

		<main>
			<section class="about">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 aboutus">
							<div class="row">
								<div class="col-lg-8 us">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/us.jpeg" alt="BBGo csapat képe.">
								</div>
								<div class="col-lg-4 nyitvatartas">
									<h3>Nyitvatartás</h3>
									<table>
									  <tbody>
									    <tr>
									      <td>Hétfő</td>
									      <td class="text-right">06:30 - 21:00</td>
									    </tr>
									    <tr>
									      <td>Kedd</td>
									      <td class="text-right">06:30 - 21:00</td>
									    </tr>
									    <tr>
									      <td>Szerda</td>
									      <td class="text-right">06:30 - 21:00</td>
									    </tr>
									    <tr>
									      <td>Csütörtök</td>
									      <td class="text-right">06:30 - 21:00</td>
									    </tr>
									    <tr>
									      <td>Péntek</td>
									      <td class="text-right">06:30 - 21:00</td>
									    </tr>
									    <tr>
									      <td>Szombat</td>
									      <td class="text-right">08:00 - 18:00</td>
									    </tr>
									    <tr>
									      <td>Vasárnap</td>
									      <td class="text-right">08:00 - 18:00</td>
									    </tr>
									  </tbody>
									</table>
								</div>
								<div class="col-lg-12 us-description">
									<h3>Mert testünk csak egy van</h3>
									<p>Életünk alapja testünk és lelkünk egészsége és egyensúlya. Nálunk mindent megtalálsz, ami az egészséges életmód fenntartásához szükséges. Termeink a legmodernebb fitnessz és kardio eszközökkel vannak felszerelve, ahol vendégeink csoportos órákon is részt vehetnek.<br>Külön figyelmet fordítunk a kismamák, terhes anyukák és gyermekeik egészségének fenntartására.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3 orarend">
							<h3>Legközelebbi óráink</h3>
							<table class="table">
							  <tbody>
							    <tr>
							      <td><strong>Gerinctorna</strong><br>csütörtök 17:30 - 18:25</td>
							    </tr>
							    <tr>
							      <td><strong>Alakformáló</strong><br>csütörtök 17:30 - 18:25</td>
							    </tr>
							    <tr>
							      <td><strong>Terhestorna</strong><br>csütörtök 18:30 - 19:25</td>
							    </tr>
							    <tr>
							      <td><strong>Kondi step</strong><br>csütörtök 18:30 - 19:25</td>
							    </tr>
							    <tr>
							      <td><strong>Pilates</strong><br>csütörtök 19:30 - 20:25</td>
							    </tr>
							    <tr>
							      <td><strong>Alakformáló</strong><br>csütörtök 19:30 - 20:25</td>
							    </tr>
							  </tbody>
							</table>
						</div>						
					</div>					
				</div>				
			</section>
			<section class="szolgaltatasok blurredbg">
				<div class="gradiented">
					<div class="container skewback">
						<h2 class="text-center">szolgáltatásaink</h2>
						<div class="row">	
							<div class="card col-lg-4 col-md-12">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fitness-icon.png" alt="Fitnessz icon.">
								<h3>általános fitnessz</h3>
							  <div>
							  	<p class="text-center">A lehetőségek tárháza szinte kimeríthetetlen. Aerobic, zumba fitness, stretching, zsírégető és alakformáló órák egyéni vagy csoportos tréningalkalmainkon.</p>
							  </div>
							</div>
							<div class="card col-lg-4 col-md-12">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tanfolyamok-icon.png" alt="Tanfolyamok icon.">
								<h3>tanfolyamok</h3>
							  <div>
							  	<p class="text-center">Képezd magad baba masszázs, baba elsősegély és szülésfelkészítő tanfolyamainkon. Szívesen oktatnál terhestornát várandós anyukáknak? A legjobb helyen jársz.</p>
							  </div>
							</div>
							<div class="card col-lg-4 col-md-12">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/kismamatorna-icon.png" alt="Kismamatorna icon.">
								<h3>terhestorna, kismamatorna</h3>
							  <div>
							  	<p class="text-center">Kismamaként sem kell lemondanod az alakodról! Oktatóink vezetésével a terhestorna mellett kipróbálhatod forradalmi módszereinket is, mint a jóga alapú kismamatorna, vagy a babás jóga.</p>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="tanfolyamok" id="tanfolyamok">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="text-center">tanfolyamaink</h2>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="card-1">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">női személyi edző tanfolyam</h3>
						    <p class="card-text text-center">"A jó személyi edző motiváló erő, szakember, lélekgyógyász..."</p>
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="card-2">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">baba elsősegély  tanfolyam budapest</h3>
						    <p class="card-text text-center">Mert olyan kisgyermek nincs, aki sosem esik el.</p>
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="card-3">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">terhestorna oktató tanfolyam</h3>
						    <p class="card-text text-center">Az első és egyetlen európa szerte.</p>
						  </div>
						</div>
						<div class="card col-lg-6 col-md-12" style="width: 100%;">
							<div class="backgrounded" id="card-4">
								<div class="blended">
								</div>
							</div>
						  <div class="card-body">
						    <h3 class="card-title text-center">szülésfelkészítő tanfolyam zuglóban</h3>
						    <p class="card-text text-center">Apás gyakorlati szülésfelkészítő tanfolyam pároknak.</p>
						  </div>
						</div>
					</div>
				</div>
			</section>
			<section class="velemenyek blurredbg">
				<div class="gradiented">
					<div class="container skewback">
						<h2 class="text-center">rólunk mondták</h2>
						<div class="row">	
							<div class="card col-lg-4 col-md-12">
							  <div class="speech-bubble">
							  	<p>Kedvenc alkalmaim a kick-box aerobic, a zumba fitness, a zsírégető- és a kondi step. Mióta ide járok, jóval energikusabbnak és magabiztosabbnak érzem magam!</p>
							  </div>
							  <div class="quoterinfo">
								  <div>
								  	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/quoter-1.jpg" alt="Egy nő képe">
								  </div>
									<div class="quoteridentity">
								   	<p class="name">angela blondye</p>
								   	<p>fitnessz vendég</p>
								  </div>
								</div>
							</div>
							<div class="card col-lg-4 col-md-12">
							  <div class="speech-bubble">
							  	<p>Aktív sportolóként lételemem a mozgás és a tréning. Egy ismerősöm ajánlotta ezt a klubot, azóta minden nap ide járok. Az oktatók is nagyon segítőkészek, öt csillag!</p>
							  </div>
							  <div class="quoterinfo">
								  <div>
								  	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/quoter-2.jpeg" alt="Egy nő képe.">
								  </div>
									<div class="quoteridentity">
								   	<p class="name">tannis batt</p>
								   	<p>haladó teniszező</p>
								  </div>
								</div>
							</div>
							<div class="card col-lg-4 col-md-12">
							  <div class="speech-bubble">
							  	<p>Nagyon szeretek jógázni, de nem gondoltam, hogy kismamaként is folytathatom. Ha a jóga alapú kismamatorna szenzációs élmény, milyen lesz a babás jóga? Alig várom!</p>
							  </div>
							  <div class="quoterinfo">
								  <div>
								  	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/quoter-3.jpeg" alt="Egy nő képe.">
								  </div>
									<div class="quoteridentity">
								   	<p class="name">alice cloudhair</p>
								   	<p>boldog kismama</p>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="montage">
				<div class="row">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alsodiv.png" alt="Fitnessz montázs.">
				</div>
			</section>
		</main>
<?php
	include "footer.php";
?>	