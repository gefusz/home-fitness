<?php
/*
	Template Name: Index Default
*/
?>

<?php
	include "header.php";
?>
		<main>
			<section class="kapcsolat">
				<div class="container content-pull">
					<div class="row">
						<div class="col-sm-12">
							<?php while ( have_posts() ) : ?>
								<?php the_post(); ?>

								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

									<?php if ( has_post_thumbnail() ) : ?>
										<div class="entry-featured">
											<?php the_post_thumbnail(); ?>
										</div>
									<?php endif; ?>

									<div class="entry-wrap">
										<header class="entry-header">
											<h2 class="text-center"><?php the_title(); ?></h1>

											<div class="entry-meta text-center">
												<time><?php the_date(); ?></time>
												<span><?php the_author(); ?></span>
											</div>
										</header>

										<div class="entry-content">
											<?php the_excerpt(); ?>
											<p class="text-center">
												<a href="<?php the_permalink(); ?>">Olvass tovább</a>
											</p>
										</div>

										<footer class="entry-footer">
											<?php the_tags(); ?>
										</footer>
									</div>
								</article><!-- #post-<?php the_ID(); ?> -->

							<?php endwhile; ?>

						</div>
					</div>
				</div>
			</section>
		</main>

<?php
	include "footer.php";
?>	