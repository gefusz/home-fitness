<!DOCTYPE html>
<html lang="hu">
	<head>
		<title>Női fitness | Főoldal.</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	   <!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- LATO font-family -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin-ext" rel="stylesheet">
<!-- PLAYFAIR font-family -->
		<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon">

		<!-- custom CSS -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom.css">
		<?php wp_head(); ?>
	</head>
	<body>
		<header>
<!-- contact information -->
			<section class="header">
				<div id="carouselExampleSlidesOnly" class="carousel slide header" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header-slider-1.jpeg" class="d-block w-100" alt="Fitnesszező nők.">
				    </div>
				    <div class="carousel-item">
				      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header-slider-2.jpeg" class="d-block w-100" alt="Fitnesszező nők.">
				    </div>
				    <div class="carousel-item">
				      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header-slider-3.jpg" class="d-block w-100" alt="Fitnesszező nők.">
				    </div>
				  </div>
				  <div class="gradiented-bg-mobile">
					  <div class="gradiented-hero-bg">
							<div class="container">
								<div class="row">
									<div class="col-md-8 hero">
										<div class="row">
											<div class="col-lg-4 col-sm-5 mainlogo">
												<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bbgo-logo.png" alt="Fitness oldal logo."></a>
											</div>
											<div class="col-lg-8">
												<ul class="d-none d-lg-flex topbar">
													<li><a href="mailto:info@noifitnesz.hu"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mail.png" alt="Mail icon."><span>info@noifitnesz.hu</span></a></li>
													<li><a href="tel:0613646788"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/phone.png" alt="Phone icon."><span>+36(1) 364 67 88</span></a></li>
													<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clock.png" alt="Clock icon."><span>H-P: 06:30 - 21:00</span></li>
												</ul>
											</div>	
											<div class="jumbotron">
											  <div class="container">
											    <h1 class="display-4">Alakformálás<br><strong>Felsőfokon</strong></h1>
											    <p class="lead">Tartsd formában az alakod. Nálunk minden szükséges eszközt megkapsz ehhez. Személyi edzőink és oktatóink gondoskodnak arról, hogy akár rövid időn belül is látványos eredményeket érj el.</p>
											  </div>
											</div>
										</div>
									</div>
									<div class="col-md-4 menu">
										<div class="pos-f-t">
											<nav class="navbar navbar-dark d-block d-md-none">
										    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
										      <span class="navbar-toggler-icon"></span>
										    </button>
										  </nav>
										  <div class="collapse" id="navbarToggleExternalContent">
										  	<div class="p-4">
											  	<?php 
										            $args = array(
										              'menu'        => 'header-menu',
										              'menu_class'  => 'nav navbar-nav',
										              'container'   => 'false'
										            );
										            wp_nav_menu( $args );
										        
										        ?>
									        </div>
										    <!-- 
										      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
											      <li class="nav-item active">
											        <a class="nav-link" href="index.html">Főoldal<span class="sr-only">(current)</span></a>
											      </li>
											      <li class="nav-item">
											        <a class="nav-link" href="foglalkozasok.html">Foglalkozások</a>
											      </li>
											      <li class="nav-item">
											        <a class="nav-link" href="#tanfolyamok">Tanfolyamok</a>
											      </li>
											      <li class="nav-item">
											        <a class="nav-link" href="fitnessplus.html">Fitness plus</a>
											      </li>
											      <li class="nav-item">
											        <a class="nav-link" href="kapcsolat.html">Kapcsolat</a>
											      </li>
										    	</ul>
										     -->
										    
										  </div>										  
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</header>