	<footer>
		<div class="gradiented">
			<div class="row">
				<div class="col-lg-6 white">
					<div class="col-md-12 col-lg-6 footer-paragraph">
						<img class="bottomlogo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/bbgo-logo-pink.png" alt="BBGo Női Fitness logoja.">
						<p>A mozgás és testünk kondíciójának fenntartása minden életkorban és élethelyzetben elengedhetetlen. Ennek ad teret létesítményünk hölgyek és kisgyermekeik számára.</p>
					</div>
					<img class="d-none d-lg-block bottomimage" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerimage.png" alt="Fitnesszező nő.">
				</div>
				<div class="col-lg-6 transparent">
					<div class="row">
						<div class="col-md-6">
							<h2>oldaltérkép</h2>
							<nav id="footer-navigation" class="site-navigation footer-navigation" role="navigation">
						       	<?php
								    wp_nav_menu( array( 
								    'theme_location' => 'footer-menu', 
								    'container_class' => 'custom-menu-class' ) ); 
								?>
							</nav>
							<!-- <ul>
								<li><a href="index.html">Főoldal</a></li>
								<li><a href="foglalkozasok.html">Foglalkozások</a></li>
								<li><a href="#tanfolyamok">Tanfolyamok</a></li>
								<li><a href="fitnessplus.html">Fitness plus</a></li>
								<li><a href="kapcsolat.html">Kapcsolat</a></li>
							</ul> -->
						</div>
						<div class="col-md-6">
							<h2>kapcsolat</h2>
							<ul>									
								<li><a href="tel:0613646788"><span>+36(1) 364 67 88</span></a></li>
								<li><a href="mailto:info@noifitnesz.hu"><span>info@noifitnesz.hu</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bottombar">
				<p class="text-center text-white">&copy; 2017 Kriston Gergő - All Rights Reserved.</p>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>