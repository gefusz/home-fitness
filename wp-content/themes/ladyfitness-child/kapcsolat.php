<?php
/*
	Template Name: Kapcsolat
*/
?>

<?php
	include "header.php";
?>
		<main>
			<section class="kapcsolat">
				<div class="container content-pull">
					<div class="row">
						<div class="col-sm-12">
							<h2 class="text-center">BBGo Női Fitness és Mozgás Stúdió</h2>
							<p><strong>Örs vezér tértől a 62-es és 3-as villamossal és a 32-es busszal is 4 megálló (6 perc)<br>A Bosnyák tértől 62-es és 3-as villamossal és a 32-es busszal is 2 megálló (4 perc)</strong><br><br><strong>Cím:</strong><br><em>1149 Budapest, Nagy Lajos Király Útja 110. (Király Udvar)</em><br><strong>Internet:</strong><br><em><a href="https://www.facebook.com/bbgo.noi.fitness/" target="blank">https://www.facebook.com/bbgo.noi.fitness/</a></em><br><strong>Email:</strong><br><em><a href="mailto:info@noifitnesz.hu">info@noifitnesz.hu</a></em><p>A mindennapi munka során sokan szinte alig mozogunk - mozgáshiány uralja napjainkat: ülünk az irodában, az autóban és esetenként a televízió előtt. A sportra és a mozgásra ezért igen nagy szükségünk van. A sport és a mozgás egyben a legjobb megelőzési módja a civilizációs betegségeknek is, melyek a mozgásszegény életmód következtében alakultak ki. Hogy testileg egészségesek és edzettek legyünk és maradjunk, ajánlott a rendszeres testmozgás.</p>
						</div>
					</div>
				</div>
			</section>
			<section class="contact-form" id="form">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3>Írj nekünk</h3>
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-3"></div>
						<div class="col-lg-6">
							<?php
								echo do_shortcode( '[contact-form-7 id="29" title="Contact form 1"]'); 
							?>
						</div>
						<div class="col-lg-3"></div>
					</div> 
				</div>
			</section>
			<section class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2694.6894102996584!2d19.116165315818566!3d47.51543960226375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741db67aaac0299%3A0xcd70c09023093f4e!2zQkJHbyBOxZFpIEZpdG5lc3Mgw6lzIE1vemfDoXMgU3TDumRpw7M!5e0!3m2!1sen!2shu!4v1550236928880" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</section>
		</main>

<?php
	include "footer.php";
?>	